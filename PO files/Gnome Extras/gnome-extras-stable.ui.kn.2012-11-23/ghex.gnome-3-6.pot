# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=ghex&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-11-18 10:28+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../data/ghex.desktop.in.h:1 ../src/ghex-window.c:664
#: ../src/ghex-window.c:1100
msgid "GHex"
msgstr ""

#: ../data/ghex.desktop.in.h:2
msgid "Hex Editor"
msgstr ""

#: ../data/ghex.desktop.in.h:3
msgid "Inspect and edit binary files"
msgstr ""

#: ../src/chartable.c:147
msgid "ASCII"
msgstr ""

#: ../src/chartable.c:147
msgid "Hex"
msgstr ""

#: ../src/chartable.c:147 ../src/preferences.c:142
msgid "Decimal"
msgstr ""

#: ../src/chartable.c:148
msgid "Octal"
msgstr ""

#: ../src/chartable.c:148
msgid "Binary"
msgstr ""

#: ../src/chartable.c:164
msgid "Character table"
msgstr ""

#: ../src/converter.c:232
msgid "Base Converter"
msgstr ""

#. entries
#: ../src/converter.c:253
msgid "_Binary:"
msgstr ""

#: ../src/converter.c:255
msgid "_Octal:"
msgstr ""

#: ../src/converter.c:257
msgid "_Decimal:"
msgstr ""

#: ../src/converter.c:259
msgid "_Hex:"
msgstr ""

#: ../src/converter.c:261
msgid "_ASCII:"
msgstr ""

#. get cursor button
#: ../src/converter.c:265
msgid "_Get cursor value"
msgstr ""

#: ../src/converter.c:279
msgid "Get cursor value"
msgstr ""

#: ../src/converter.c:279
msgid "Gets the value at cursor in binary, octal, decimal, hex and ASCII"
msgstr ""

#: ../src/converter.c:419
msgid "ERROR"
msgstr ""

#: ../src/findreplace.c:97 ../src/findreplace.c:221 ../src/ui.c:874
#, c-format
msgid "GHex (%s): Find Data"
msgstr ""

#: ../src/findreplace.c:101 ../src/findreplace.c:161 ../src/findreplace.c:359
msgid "Find String"
msgstr ""

#: ../src/findreplace.c:108 ../src/findreplace.c:258
msgid "Find _Next"
msgstr ""

#: ../src/findreplace.c:115 ../src/findreplace.c:267
msgid "Find _Previous"
msgstr ""

#: ../src/findreplace.c:138 ../src/findreplace.c:413
msgid "Find Data"
msgstr ""

#: ../src/findreplace.c:138 ../src/findreplace.c:413
msgid "Enter the hex data or ASCII data to search for"
msgstr ""

#: ../src/findreplace.c:139
msgid "Find Next"
msgstr ""

#: ../src/findreplace.c:139 ../src/findreplace.c:415
msgid "Finds the next occurrence of the search string"
msgstr ""

#: ../src/findreplace.c:140
msgid "Find previous"
msgstr ""

#: ../src/findreplace.c:140
msgid "Finds the previous occurrence of the search string "
msgstr ""

#: ../src/findreplace.c:141 ../src/findreplace.c:418 ../src/findreplace.c:469
msgid "Cancel"
msgstr ""

#: ../src/findreplace.c:141
msgid "Closes find data window"
msgstr ""

#: ../src/findreplace.c:157
#, c-format
msgid "GHex (%s): Find Data: Add search"
msgstr ""

#: ../src/findreplace.c:179
msgid "Add"
msgstr ""

#: ../src/findreplace.c:235
msgid "Search String"
msgstr ""

#: ../src/findreplace.c:243
msgid "Highlight Colour"
msgstr ""

#: ../src/findreplace.c:280
msgid "_Add New"
msgstr ""

#: ../src/findreplace.c:289
msgid "_Remove Selected"
msgstr ""

#: ../src/findreplace.c:309
msgid "Close"
msgstr ""

#: ../src/findreplace.c:309
msgid "Closes advanced find window"
msgstr ""

#: ../src/findreplace.c:355 ../src/ui.c:872
#, c-format
msgid "GHex (%s): Find & Replace Data"
msgstr ""

#: ../src/findreplace.c:368
msgid "Replace With"
msgstr ""

#: ../src/findreplace.c:375
msgid "Find _next"
msgstr ""

#: ../src/findreplace.c:383
msgid "_Replace"
msgstr ""

#: ../src/findreplace.c:391
msgid "Replace _All"
msgstr ""

#: ../src/findreplace.c:414
msgid "Replace Data"
msgstr ""

#: ../src/findreplace.c:414
msgid "Enter the hex data or ASCII data to replace with"
msgstr ""

#: ../src/findreplace.c:415
msgid "Find next"
msgstr ""

#: ../src/findreplace.c:416
msgid "Replace"
msgstr ""

#: ../src/findreplace.c:416
msgid "Replaces the search string with the replace string"
msgstr ""

#: ../src/findreplace.c:417
msgid "Replace All"
msgstr ""

#: ../src/findreplace.c:417
msgid "Replaces all occurrences of the search string with the replace string"
msgstr ""

#: ../src/findreplace.c:418
msgid "Closes find and replace data window"
msgstr ""

#: ../src/findreplace.c:434 ../src/ui.c:870
#, c-format
msgid "GHex (%s): Jump To Byte"
msgstr ""

#: ../src/findreplace.c:467
msgid "Jump to byte"
msgstr ""

#: ../src/findreplace.c:467
msgid "Enter the byte to jump to"
msgstr ""

#: ../src/findreplace.c:468
msgid "OK"
msgstr ""

#: ../src/findreplace.c:468
msgid "Jumps to the specified byte"
msgstr ""

#: ../src/findreplace.c:469
msgid "Closes jump to byte window"
msgstr ""

#: ../src/findreplace.c:532 ../src/findreplace.c:566 ../src/findreplace.c:673
msgid "There is no active document to search!"
msgstr ""

#: ../src/findreplace.c:539 ../src/findreplace.c:573 ../src/findreplace.c:680
#: ../src/findreplace.c:714 ../src/findreplace.c:759
msgid "There is no string to search for!"
msgstr ""

#: ../src/findreplace.c:551 ../src/findreplace.c:689 ../src/findreplace.c:879
msgid "End Of File reached"
msgstr ""

#: ../src/findreplace.c:552 ../src/findreplace.c:585 ../src/findreplace.c:688
#: ../src/findreplace.c:880 ../src/findreplace.c:903
msgid "String was not found!\n"
msgstr ""

#: ../src/findreplace.c:584 ../src/findreplace.c:902
msgid "Beginning Of File reached"
msgstr ""

#: ../src/findreplace.c:601
msgid "There is no active document to move the cursor in!"
msgstr ""

#: ../src/findreplace.c:619
msgid "No offset has been specified!"
msgstr ""

#: ../src/findreplace.c:644
msgid "The specified offset is beyond the  file boundaries!"
msgstr ""

#: ../src/findreplace.c:652
msgid "Can not position cursor beyond the End Of File!"
msgstr ""

#: ../src/findreplace.c:659
msgid ""
"You may only give the offset as:\n"
"  - a positive decimal number, or\n"
"  - a hex number, beginning with '0x', or\n"
"  - a '+' or '-' sign, followed by a relative offset"
msgstr ""

#: ../src/findreplace.c:705
msgid "There is no active buffer to replace data in!"
msgstr ""

#: ../src/findreplace.c:730 ../src/findreplace.c:731
msgid "End Of File reached!"
msgstr ""

#: ../src/findreplace.c:750
msgid "There is no active document to replace data in!"
msgstr ""

#: ../src/findreplace.c:780
msgid "No occurrences were found."
msgstr ""

#: ../src/findreplace.c:783
#, c-format
msgid "Replaced %d occurrence."
msgid_plural "Replaced %d occurrences."
msgstr[0] ""
msgstr[1] ""

#: ../src/findreplace.c:823
msgid "No string to search for!"
msgstr ""

#: ../src/ghex-window.c:90
#, c-format
msgid ""
"Can not open URI:\n"
"%s"
msgstr ""

#: ../src/ghex-window.c:111
#, c-format
msgid ""
"Can not open file:\n"
"%s"
msgstr ""

#: ../src/ghex-window.c:340
msgid "_File"
msgstr ""

#: ../src/ghex-window.c:341
msgid "_Edit"
msgstr ""

#: ../src/ghex-window.c:342
msgid "_View"
msgstr ""

#: ../src/ghex-window.c:343
msgid "_Group Data As"
msgstr ""

#. View submenu
#: ../src/ghex-window.c:344
msgid "_Windows"
msgstr ""

#: ../src/ghex-window.c:345
msgid "_Help"
msgstr ""

#. File menu
#: ../src/ghex-window.c:348
msgid "_Open..."
msgstr ""

#: ../src/ghex-window.c:349
msgid "Open a file"
msgstr ""

#: ../src/ghex-window.c:351
msgid "_Save"
msgstr ""

#: ../src/ghex-window.c:352
msgid "Save the current file"
msgstr ""

#: ../src/ghex-window.c:354
msgid "Save _As..."
msgstr ""

#: ../src/ghex-window.c:355
msgid "Save the current file with a different name"
msgstr ""

#: ../src/ghex-window.c:357
msgid "Save As _HTML..."
msgstr ""

#: ../src/ghex-window.c:358
msgid "Export data to HTML source"
msgstr ""

#: ../src/ghex-window.c:360
msgid "_Revert"
msgstr ""

#: ../src/ghex-window.c:361
msgid "Revert to a saved version of the file"
msgstr ""

#: ../src/ghex-window.c:363
msgid "_Print"
msgstr ""

#: ../src/ghex-window.c:364
msgid "Print the current file"
msgstr ""

#: ../src/ghex-window.c:366
msgid "Print Previe_w..."
msgstr ""

#: ../src/ghex-window.c:367
msgid "Preview printed data"
msgstr ""

#: ../src/ghex-window.c:369
msgid "_Close"
msgstr ""

#: ../src/ghex-window.c:370
msgid "Close the current file"
msgstr ""

#: ../src/ghex-window.c:372
msgid "E_xit"
msgstr ""

#: ../src/ghex-window.c:373
msgid "Exit the program"
msgstr ""

#. Edit menu
#: ../src/ghex-window.c:377
msgid "_Undo"
msgstr ""

#: ../src/ghex-window.c:378
msgid "Undo the last action"
msgstr ""

#: ../src/ghex-window.c:380
msgid "_Redo"
msgstr ""

#: ../src/ghex-window.c:381
msgid "Redo the undone action"
msgstr ""

#: ../src/ghex-window.c:383
msgid "_Copy"
msgstr ""

#: ../src/ghex-window.c:384
msgid "Copy selection to clipboard"
msgstr ""

#: ../src/ghex-window.c:386
msgid "Cu_t"
msgstr ""

#: ../src/ghex-window.c:387
msgid "Cut selection"
msgstr ""

#: ../src/ghex-window.c:389
msgid "Pa_ste"
msgstr ""

#: ../src/ghex-window.c:390
msgid "Paste data from clipboard"
msgstr ""

#: ../src/ghex-window.c:392
msgid "_Find"
msgstr ""

#: ../src/ghex-window.c:393
msgid "Search for a string"
msgstr ""

#: ../src/ghex-window.c:395
msgid "_Advanced Find"
msgstr ""

#: ../src/ghex-window.c:396
msgid "Advanced Find"
msgstr ""

#: ../src/ghex-window.c:398
msgid "R_eplace"
msgstr ""

#: ../src/ghex-window.c:399
msgid "Replace a string"
msgstr ""

#: ../src/ghex-window.c:401
msgid "_Goto Byte..."
msgstr ""

#: ../src/ghex-window.c:402
msgid "Jump to a certain position"
msgstr ""

#: ../src/ghex-window.c:404
msgid "_Preferences"
msgstr ""

#: ../src/ghex-window.c:405
msgid "Configure the application"
msgstr ""

#. View menu
#: ../src/ghex-window.c:409
msgid "_Add View"
msgstr ""

#: ../src/ghex-window.c:410
msgid "Add a new view to the buffer"
msgstr ""

#: ../src/ghex-window.c:412
msgid "_Remove View"
msgstr ""

#: ../src/ghex-window.c:413
msgid "Remove the current view of the buffer"
msgstr ""

#. Help menu
#: ../src/ghex-window.c:417
msgid "_Contents"
msgstr ""

#: ../src/ghex-window.c:418
msgid "Help on this application"
msgstr ""

#: ../src/ghex-window.c:420
msgid "_About"
msgstr ""

#: ../src/ghex-window.c:421
msgid "About this application"
msgstr ""

#. Edit menu
#: ../src/ghex-window.c:428
msgid "_Insert Mode"
msgstr ""

#: ../src/ghex-window.c:429
msgid "Insert/overwrite data"
msgstr ""

#. Windows menu
#: ../src/ghex-window.c:433
msgid "Character _Table"
msgstr ""

#: ../src/ghex-window.c:434
msgid "Show the character table"
msgstr ""

#: ../src/ghex-window.c:436
msgid "_Base Converter"
msgstr ""

#: ../src/ghex-window.c:437
msgid "Open base conversion dialog"
msgstr ""

#: ../src/ghex-window.c:439
msgid "Type Conversion _Dialog"
msgstr ""

#: ../src/ghex-window.c:440
msgid "Show the type conversion dialog in the edit window"
msgstr ""

#: ../src/ghex-window.c:446 ../src/ui.c:48
msgid "_Bytes"
msgstr ""

#: ../src/ghex-window.c:447
msgid "Group data by 8 bits"
msgstr ""

#: ../src/ghex-window.c:448 ../src/ui.c:49
msgid "_Words"
msgstr ""

#: ../src/ghex-window.c:449
msgid "Group data by 16 bits"
msgstr ""

#: ../src/ghex-window.c:450 ../src/ui.c:50
msgid "_Longwords"
msgstr ""

#: ../src/ghex-window.c:451
msgid "Group data by 32 bits"
msgstr ""

#: ../src/ghex-window.c:785
#, c-format
msgid "Offset: %s"
msgstr ""

#: ../src/ghex-window.c:788
#, c-format
msgid "; %s bytes from %s to %s selected"
msgstr ""

#: ../src/ghex-window.c:1060
#, c-format
msgid "Activate file %s"
msgstr ""

#: ../src/ghex-window.c:1096
#, c-format
msgid "%s - GHex"
msgstr ""

#: ../src/ghex-window.c:1218
msgid "Select a file to save buffer as"
msgstr ""

#: ../src/ghex-window.c:1250
#, c-format
msgid ""
"File %s exists.\n"
"Do you want to overwrite it?"
msgstr ""

#: ../src/ghex-window.c:1276 ../src/ui.c:300
#, c-format
msgid "Saved buffer to file %s"
msgstr ""

#: ../src/ghex-window.c:1283
msgid "Error saving file!"
msgstr ""

#: ../src/ghex-window.c:1289
msgid "Can't open file for writing!"
msgstr ""

#: ../src/ghex-window.c:1334
#, c-format
msgid ""
"File %s has changed since last save.\n"
"Do you want to save changes?"
msgstr ""

#: ../src/ghex-window.c:1338
msgid "Do_n't save"
msgstr ""

#: ../src/ghex-window.c:1358 ../src/ui.c:288
msgid "You don't have the permissions to save the file!"
msgstr ""

#: ../src/ghex-window.c:1362 ../src/ui.c:293
msgid "An error occurred while saving file!"
msgstr ""

#: ../src/hex-dialog.c:58
msgid "Signed 8 bit:"
msgstr ""

#: ../src/hex-dialog.c:59
msgid "Unsigned 8 bit:"
msgstr ""

#: ../src/hex-dialog.c:60
msgid "Signed 16 bit:"
msgstr ""

#: ../src/hex-dialog.c:61
msgid "Unsigned 16 bit:"
msgstr ""

#: ../src/hex-dialog.c:62
msgid "Signed 32 bit:"
msgstr ""

#: ../src/hex-dialog.c:63
msgid "Unsigned 32 bit:"
msgstr ""

#: ../src/hex-dialog.c:64
msgid "Float 32 bit:"
msgstr ""

#: ../src/hex-dialog.c:65
msgid "Float 64 bit:"
msgstr ""

#: ../src/hex-dialog.c:66
msgid "Hexadecimal:"
msgstr ""

#: ../src/hex-dialog.c:67
msgid "Octal:"
msgstr ""

#: ../src/hex-dialog.c:68
msgid "Binary:"
msgstr ""

#: ../src/hex-dialog.c:204
msgid "Show little endian decoding"
msgstr ""

#: ../src/hex-dialog.c:211
msgid "Show unsigned and float as hexadecimal"
msgstr ""

#: ../src/hex-dialog.c:217
msgid "Stream Length:"
msgstr ""

#: ../src/hex-dialog.c:239
msgid "FIXME: no conversion function"
msgstr ""

#: ../src/hex-document.c:435
msgid "New document"
msgstr ""

#: ../src/hex-document.c:784
#, c-format
msgid "Page"
msgstr ""

#: ../src/hex-document.c:790 ../src/hex-document.c:915
#, c-format
msgid "Hex dump generated by"
msgstr ""

#: ../src/hex-document.c:801
msgid "Saving to HTML..."
msgstr ""

#: ../src/hex-document.c:838
#, c-format
msgid "Previous page"
msgstr ""

#: ../src/hex-document.c:853
#, c-format
msgid "Next page"
msgstr ""

#: ../src/main.c:36
msgid "X geometry specification (see \"X\" man page)."
msgstr ""

#: ../src/main.c:36
msgid "GEOMETRY"
msgstr ""

#: ../src/main.c:37
msgid "FILES"
msgstr ""

#: ../src/main.c:99
msgid "- GTK+ binary editor"
msgstr ""

#: ../src/main.c:103
#, c-format
msgid ""
"%s\n"
"Run '%s --help' to see a full list of available command line options.\n"
msgstr ""

#: ../src/main.c:133 ../src/main.c:146
#, c-format
msgid "Invalid geometry string \"%s\"\n"
msgstr ""

#: ../src/preferences.c:69
msgid "GHex Preferences"
msgstr ""

#: ../src/preferences.c:101
msgid "_Maximum number of undo levels:"
msgstr ""

#: ../src/preferences.c:121
msgid "Undo levels"
msgstr ""

#: ../src/preferences.c:121
msgid "Select maximum number of undo levels"
msgstr ""

#: ../src/preferences.c:125
msgid "_Show cursor offset in statusbar as:"
msgstr ""

#: ../src/preferences.c:144
msgid "Hexadecimal"
msgstr ""

#: ../src/preferences.c:146
msgid "Custom"
msgstr ""

#: ../src/preferences.c:155
msgid "Enter the cursor offset format"
msgstr ""

#: ../src/preferences.c:156
msgid "Select the cursor offset format"
msgstr ""

#. show offsets check button
#: ../src/preferences.c:166
msgid "Sh_ow offsets column"
msgstr ""

#: ../src/preferences.c:171
msgid "Editing"
msgstr ""

#. display font
#: ../src/preferences.c:180
msgid "Font"
msgstr ""

#. default group type
#: ../src/preferences.c:203
msgid "Default Group Type"
msgstr ""

#: ../src/preferences.c:219
msgid "Display"
msgstr ""

#. paper selection
#: ../src/preferences.c:228
msgid "Paper size"
msgstr ""

#. data & header font selection
#: ../src/preferences.c:233
msgid "Fonts"
msgstr ""

#: ../src/preferences.c:245
msgid "_Data font:"
msgstr ""

#: ../src/preferences.c:259
msgid "Data font"
msgstr ""

#: ../src/preferences.c:259
msgid "Select the data font"
msgstr ""

#: ../src/preferences.c:268
msgid "Header fo_nt:"
msgstr ""

#: ../src/preferences.c:281
msgid "Header font"
msgstr ""

#: ../src/preferences.c:281
msgid "Select the header font"
msgstr ""

#: ../src/preferences.c:306
msgid "_Print shaded box over:"
msgstr ""

#: ../src/preferences.c:318
msgid "Box size"
msgstr ""

#: ../src/preferences.c:318
msgid "Select size of box (in number of lines)"
msgstr ""

#: ../src/preferences.c:322
msgid "lines (0 for no box)"
msgstr ""

#: ../src/preferences.c:329
msgid "Printing"
msgstr ""

#: ../src/preferences.c:447 ../src/ui.c:209
#, c-format
msgid ""
"There was an error displaying help: \n"
"%s"
msgstr ""

#: ../src/preferences.c:499
msgid "Can not open desired font!"
msgstr ""

#: ../src/preferences.c:560
msgid ""
"The offset format string contains invalid format specifier.\n"
"Only 'x', 'X', 'p', 'P', 'd' and 'o' are allowed."
msgstr ""

#: ../src/print.c:57
#, c-format
msgid "Page: %i/%i"
msgstr ""

#: ../src/ui.c:55
msgid "hex data"
msgstr ""

#: ../src/ui.c:56
msgid "ASCII data"
msgstr ""

#: ../src/ui.c:150
msgid ""
"This program is free software; you can redistribute it and/or modify it "
"under the terms of the GNU General Public License as published by the Free "
"Software Foundation; either version 2 of the License, or (at your option) "
"any later version."
msgstr ""

#: ../src/ui.c:154
msgid ""
"This program is distributed in the hope that it will be useful, but WITHOUT "
"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
"FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
"more details."
msgstr ""

#: ../src/ui.c:158
msgid ""
"You should have received a copy of the GNU General Public License along with "
"this program; if not, write to the Free Software Foundation, Inc., 51 "
"Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA"
msgstr ""

#. Translators: these two strings here indicate the copyright time span,
#. e.g. 1998-2012.
#: ../src/ui.c:170
msgid "Copyright © %Id–%Id The GHex authors"
msgstr ""

#: ../src/ui.c:174
msgid "A binary file editor"
msgstr ""

#: ../src/ui.c:180
msgid "About GHex"
msgstr ""

#: ../src/ui.c:181
msgid "translator-credits"
msgstr ""

#: ../src/ui.c:184
msgid "GHex Website"
msgstr ""

#: ../src/ui.c:318
msgid "Select a file to open"
msgstr ""

#: ../src/ui.c:350
#, c-format
msgid "Loaded file %s"
msgstr ""

#: ../src/ui.c:358
msgid "Can not open file!"
msgstr ""

#: ../src/ui.c:423
msgid "Select path and file name for the HTML source"
msgstr ""

#: ../src/ui.c:455
msgid "You need to specify a base name for the HTML files."
msgstr ""

#: ../src/ui.c:466 ../src/ui.c:492
msgid "You don't have the permission to write to the selected path.\n"
msgstr ""

#: ../src/ui.c:478
msgid ""
"Saving to HTML will overwrite some files.\n"
"Do you want to proceed?"
msgstr ""

#: ../src/ui.c:746
#, c-format
msgid "Really revert file %s?"
msgstr ""

#: ../src/ui.c:760
#, c-format
msgid "Reverted buffer from file %s"
msgstr ""
