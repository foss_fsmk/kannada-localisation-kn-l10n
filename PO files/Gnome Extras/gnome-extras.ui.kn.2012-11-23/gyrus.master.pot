# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=gyrus&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-11-12 01:22+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../gyrus.desktop.in.in.h:1
msgid "Gyrus IMAP Cyrus Administrator"
msgstr ""

#: ../gyrus.desktop.in.in.h:2
msgid "Administer the mailboxes of your IMAP Cyrus servers"
msgstr ""

#: ../src/ui/create_mailbox.xml.h:1
msgid "Name:"
msgstr ""

#: ../src/ui/create_mailbox.xml.h:2
msgid "Quota (MB):"
msgstr ""

#: ../src/ui/create_mailbox.xml.h:3
msgid "Assign quota"
msgstr ""

#: ../src/ui/find.xml.h:1
msgid "Search for:"
msgstr ""

#: ../src/ui/find.xml.h:2
msgid "Match _entire word only"
msgstr ""

#: ../src/ui/find.xml.h:3
msgid "_Wrap around"
msgstr ""

#: ../src/ui/page.xml.h:1
msgid "Free space:"
msgstr ""

#: ../src/ui/page.xml.h:2
msgid "Assigned space:"
msgstr ""

#: ../src/ui/page.xml.h:3
msgid "Owner:"
msgstr ""

#: ../src/ui/page.xml.h:4
msgid "New quota (MB)"
msgstr ""

#: ../src/ui/page.xml.h:5
msgid "Modify quota"
msgstr ""

#: ../src/ui/page.xml.h:6
msgid "Access control list"
msgstr ""

#: ../src/ui/page.xml.h:7 ../src/ui/sessions_edit.xml.h:5
#: ../tests/gyrus-talk.xml.h:2
msgid "Host:"
msgstr ""

#: ../src/ui/page.xml.h:8
msgid "User:"
msgstr ""

#: ../src/ui/page.xml.h:9 ../src/ui/sessions_edit.xml.h:4
#: ../tests/gyrus-talk.xml.h:4
msgid "Port:"
msgstr ""

#: ../src/ui/password.xml.h:1
msgid "Password"
msgstr ""

#: ../src/ui/password.xml.h:2
msgid "Enter your password"
msgstr ""

#: ../src/ui/report.xml.h:1
msgid "Report"
msgstr ""

#: ../src/ui/report.xml.h:3
#, no-c-format
msgid "Over (%)"
msgstr ""

#: ../src/ui/sessions.xml.h:1
msgid "Open session"
msgstr ""

#: ../src/ui/sessions_edit.xml.h:1
msgid "Session name:"
msgstr ""

#: ../src/ui/sessions_edit.xml.h:2
msgid "Password:"
msgstr ""

#: ../src/ui/sessions_edit.xml.h:3
msgid "Username:"
msgstr ""

#: ../src/ui/sessions_edit.xml.h:6
msgid "Session details"
msgstr ""

#: ../src/ui/sessions_edit.xml.h:7 ../tests/gyrus-talk.xml.h:3
msgid "Use a secure connection"
msgstr ""

#: ../src/ui/sessions_edit.xml.h:8
msgid "Mailbox hierarchy separator:"
msgstr ""

#: ../src/ui/sessions_edit.xml.h:9
msgid "<b>Options</b>"
msgstr ""

#: ../src/gyrus-admin-acl.c:54 ../src/gyrus-admin-acl.c:103
#, c-format
msgid "Mailbox '%s' does not exist."
msgstr ""

#: ../src/gyrus-admin-acl.c:76
msgid "Invalid identifier."
msgstr ""

#: ../src/gyrus-admin-acl.c:82
msgid "Empty entry name."
msgstr ""

#: ../src/gyrus-admin-acl.c:87
msgid "Empty mailbox name."
msgstr ""

#: ../src/gyrus-admin-acl.c:107
msgid "Missing required argument to Setacl"
msgstr ""

#: ../src/gyrus-admin-acl.c:143 ../src/gyrus-admin-mailbox.c:80
msgid "Permission denied"
msgstr ""

#: ../src/gyrus-admin-acl.c:175
msgid "Empty access control list."
msgstr ""

#: ../src/gyrus-admin-mailbox.c:78
msgid "Quota does not exist"
msgstr ""

#: ../src/gyrus-admin-mailbox.c:172
#, c-format
msgid "Quota overloaded"
msgstr ""

#: ../src/gyrus-admin-mailbox.c:230
msgid "Quota not valid. Please try again."
msgstr ""

#: ../src/gyrus-admin-mailbox.c:244
msgid ""
"Unable to change quota. Are you sure do you have the appropriate permissions?"
msgstr ""

#: ../src/gyrus-admin-mailbox.c:324
#, c-format
msgid "'%s' is not a valid mailbox name. Please try a different one."
msgstr ""

#: ../src/gyrus-admin-mailbox.c:332
#, c-format
msgid ""
"Parent mailbox '%s' does not exist. Please refresh the mailboxes list and "
"try again."
msgstr ""

#: ../src/gyrus-admin-mailbox.c:342
#, c-format
msgid "Mailbox '%s' already exists. Please try a different name."
msgstr ""

#: ../src/gyrus-admin-mailbox.c:355
msgid ""
"Unable to create the mailbox. Are you sure do you have the appropriate "
"permissions?"
msgstr ""

#: ../src/gyrus-admin-mailbox.c:366
msgid "Mailbox created, but could not set quota."
msgstr ""

#: ../src/gyrus-admin-mailbox.c:433
#, c-format
msgid "Unable to delete '%s'. Permission denied."
msgstr ""

#: ../src/gyrus-admin-mailbox.c:634
msgid "new entry"
msgstr ""

#: ../src/gyrus-admin.c:447 ../src/gyrus-report.c:289
#, c-format
msgid "Users (%d)"
msgstr ""

#: ../src/gyrus-admin.c:453
#, c-format
msgid "Orphaned mailboxes (%d)"
msgstr ""

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:855
msgid "Orphaned mailboxes"
msgstr ""

#: ../src/gyrus-admin.c:497 ../src/gyrus-admin.c:853 ../src/gyrus-report.c:170
msgid "Users"
msgstr ""

#: ../src/gyrus-admin.c:565
msgid "lookup"
msgstr ""

#: ../src/gyrus-admin.c:566
msgid "read"
msgstr ""

#: ../src/gyrus-admin.c:567
msgid "seen"
msgstr ""

#: ../src/gyrus-admin.c:568
msgid "write"
msgstr ""

#: ../src/gyrus-admin.c:569
msgid "insert"
msgstr ""

#: ../src/gyrus-admin.c:570
msgid "post"
msgstr ""

#: ../src/gyrus-admin.c:571
msgid "create"
msgstr ""

#: ../src/gyrus-admin.c:572
msgid "delete"
msgstr ""

#: ../src/gyrus-admin.c:573
msgid "admin"
msgstr ""

#: ../src/gyrus-admin.c:581
msgid "Identifier"
msgstr ""

#: ../src/gyrus-admin.c:651
msgid "Couldn't create the client socket."
msgstr ""

#: ../src/gyrus-admin.c:659
msgid "Couldn't parse the server address."
msgstr ""

#: ../src/gyrus-admin.c:668
#, c-format
msgid "Could not connect to %s, port %d."
msgstr ""

#: ../src/gyrus-admin.c:1001
msgid "Unable to connect with empty passwords. Please introduce your password."
msgstr ""

#: ../src/gyrus-admin.c:1008
msgid "Incorrect login/password"
msgstr ""

#: ../src/gyrus-admin.c:1364
msgid "Could not change permission. Server error: "
msgstr ""

#: ../src/gyrus-admin.c:1407
#, c-format
msgid "An entry called '%s' already exists. Overwrite it?"
msgstr ""

#: ../src/gyrus-dialog-find-mailbox.c:171
#, c-format
msgid "The text '%s' was not found in the mailbox list."
msgstr ""

#: ../src/gyrus-dialog-find-mailbox.c:275
msgid "Find mailbox"
msgstr ""

#: ../src/gyrus-dialog-mailbox-new.c:93
msgid "Quota not valid"
msgstr ""

#: ../src/gyrus-dialog-mailbox-new.c:220
msgid "New mailbox"
msgstr ""

#: ../src/gyrus-main-app.c:146
#, c-format
msgid "Really delete mailbox '%s' and all of its submailboxes?"
msgstr ""

#: ../src/gyrus-main-app.c:261 ../src/gyrus-main-app.c:436
#: ../src/gyrus-main-app.c:732
msgid "Cyrus IMAP Administrator"
msgstr ""

#: ../src/gyrus-main-app.c:275
#, c-format
msgid "%s - Cyrus IMAP Administrator"
msgstr ""

#: ../src/gyrus-main-app.c:383
msgid "_File"
msgstr ""

#: ../src/gyrus-main-app.c:384
msgid "_Edit"
msgstr ""

#: ../src/gyrus-main-app.c:385
msgid "_ACL"
msgstr ""

#: ../src/gyrus-main-app.c:386
msgid "_View"
msgstr ""

#: ../src/gyrus-main-app.c:387
msgid "_Help"
msgstr ""

#: ../src/gyrus-main-app.c:388
msgid "Go to server..."
msgstr ""

#: ../src/gyrus-main-app.c:389
msgid "Show the list of servers"
msgstr ""

#: ../src/gyrus-main-app.c:397
msgid "Add mailbox"
msgstr ""

#: ../src/gyrus-main-app.c:398
msgid "Add a mailbox under the one selected"
msgstr ""

#: ../src/gyrus-main-app.c:400
msgid "Search for a mailbox in current server"
msgstr ""

#: ../src/gyrus-main-app.c:402
msgid "Refresh the mailbox list"
msgstr ""

#: ../src/gyrus-main-app.c:403
msgid "Create report..."
msgstr ""

#: ../src/gyrus-main-app.c:404
msgid "Create report of users with quota problems"
msgstr ""

#: ../src/gyrus-main-app.c:408
msgid "New entry"
msgstr ""

#: ../src/gyrus-main-app.c:409
msgid "Create a new ACL entry in current mailbox"
msgstr ""

#: ../src/gyrus-main-app.c:410
msgid "Remove mailbox"
msgstr ""

#: ../src/gyrus-main-app.c:411
msgid "Remove current mailbox from the server"
msgstr ""

#: ../src/gyrus-main-app.c:416
msgid "Rename entry"
msgstr ""

#: ../src/gyrus-main-app.c:417
msgid "Rename selected ACL entry"
msgstr ""

#: ../src/gyrus-main-app.c:418
msgid "Delete entry"
msgstr ""

#: ../src/gyrus-main-app.c:419
msgid "Delete selected ACL entry"
msgstr ""

#: ../src/gyrus-main-app.c:552
msgid "translators-credits"
msgstr ""

#: ../src/gyrus-main-app.c:562
msgid "GNOME Cyrus Administrator"
msgstr ""

#: ../src/gyrus-main-app.c:564
msgid ""
"(c) 2003-2005 GNOME Foundation\n"
"(c) 2004-2005 Claudio Saavedra"
msgstr ""

#: ../src/gyrus-main-app.c:566
msgid "Administration tool for Cyrus IMAP servers."
msgstr ""

#. set title
#: ../src/gyrus-report.c:107
#, c-format
msgid "Mailbox space usage report for %s"
msgstr ""

#: ../src/gyrus-report.c:191 ../src/gyrus-report.c:492
msgid "Quota (%)"
msgstr ""

#: ../src/gyrus-report.c:204 ../src/gyrus-report.c:496
msgid "Assigned (KB)"
msgstr ""

#: ../src/gyrus-report.c:215 ../src/gyrus-report.c:500
msgid "Used (KB)"
msgstr ""

#. Translators: this represents the number of pages being printed.
#: ../src/gyrus-report.c:465
#, c-format
msgid "%d/%d"
msgstr ""

#: ../src/gyrus-report.c:488
msgid "User"
msgstr ""

#: ../src/gyrus-session.c:164
msgid "Edit session"
msgstr ""

#: ../src/gyrus-session.c:178
msgid "New session"
msgstr ""

#: ../src/gyrus-session.c:357
msgid "A session name is required."
msgstr ""

#: ../src/gyrus-session.c:368
#, c-format
msgid "Session named \"%s\" already exists."
msgstr ""

#: ../src/gyrus-session.c:464 ../src/gyrus-session.c:598
msgid "Autodetect"
msgstr ""

#: ../src/gyrus-session.c:549
msgid "Session"
msgstr ""

#: ../src/gyrus-session.c:718
msgid "No host specified."
msgstr ""

#: ../tests/gyrus-talk.xml.h:1
msgid "Talk - Echo client"
msgstr ""

#: ../tests/gyrus-talk.xml.h:5
msgid "_Connect"
msgstr ""

#: ../tests/gyrus-talk.xml.h:6
msgid "Connection"
msgstr ""

#: ../tests/gyrus-talk.xml.h:7
msgid "Command:"
msgstr ""

#: ../tests/gyrus-talk.xml.h:8
msgid "_Send"
msgstr ""
