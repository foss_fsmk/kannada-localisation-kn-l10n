# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-10-08 12:33+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/thumbnailer.schemas.in.h:1
msgid "Whether to enable thumbnailing of HTML files"
msgstr ""

#: ../data/thumbnailer.schemas.in.h:2
msgid "Whether to enable thumbnailing of HTML files."
msgstr ""

#: ../data/thumbnailer.schemas.in.h:3
msgid "The command to thumbnail HTML files"
msgstr ""

#: ../data/thumbnailer.schemas.in.h:4
msgid "The command to thumbnail HTML files."
msgstr ""

#: ../data/thumbnailer.schemas.in.h:5
msgid "Whether to enable thumbnailing of XHTML files"
msgstr ""

#: ../data/thumbnailer.schemas.in.h:6
msgid "Whether to enable thumbnailing of XHTML files."
msgstr ""

#: ../data/thumbnailer.schemas.in.h:7
msgid "The command to thumbnail XHTML files"
msgstr ""

#: ../data/thumbnailer.schemas.in.h:8
msgid "The command to thumbnail XHTML files."
msgstr ""

#. Translators: first %s is a URI
#: ../src/gnome-web-photo.c:376 ../src/gnome-web-photo.c:391
#, c-format
msgid "Error while saving '%s': %s\n"
msgstr ""

#. Translators: first %s is a URI
#: ../src/gnome-web-photo.c:444 ../src/gnome-web-photo.c:487
#, c-format
msgid "Error while thumbnailing '%s': %s\n"
msgstr ""

#. Translators: first %s is a URI, second %s is a printer name
#: ../src/gnome-web-photo.c:521
#, c-format
msgid "Error while printing '%s' on '%s': %s\n"
msgstr ""

#. Translators: first %s is a URI
#: ../src/gnome-web-photo.c:541
#, c-format
msgid "Error while printing '%s': %s\n"
msgstr ""

#. Translators: first %s is a URI
#: ../src/gnome-web-photo.c:622
#, c-format
msgid "Error while loading '%s': %s\n"
msgstr ""

#. Translators: first %s is a URI
#: ../src/gnome-web-photo.c:685
#, c-format
msgid "Timed out while loading '%s'. Outputting current view...\n"
msgstr ""

#. Translators: first %s is a URI
#: ../src/gnome-web-photo.c:699
#, c-format
msgid "Timed out while loading '%s'. Nothing to output...\n"
msgstr ""

#. Translators: first %s is a URI
#: ../src/gnome-web-photo.c:705
#, c-format
msgid "Timed out while loading '%s'.\n"
msgstr ""

#: ../src/gnome-web-photo.c:812
#, c-format
msgid "Unknown mode '%s'"
msgstr ""

#: ../src/gnome-web-photo.c:825
#, c-format
msgid "Usage: %s [--mode=photo|thumbnail|print] [...]\n"
msgstr ""

#: ../src/gnome-web-photo.c:829
#, c-format
msgid ""
"Usage: %s [-c CSSFILE] [-d DELAY] [-t TIMEOUT] [--force] [-w WIDTH] [--file] "
"URI|FILE OUTFILE\n"
msgstr ""

#: ../src/gnome-web-photo.c:832
#, c-format
msgid ""
"Usage: %s [-c CSSFILE] [-d DELAY] [-t TIMEOUT] [--force] [-w WIDTH] [-s "
"THUMBNAILSIZE] [--file] URI|FILE OUTFILE\n"
msgstr ""

#: ../src/gnome-web-photo.c:836
#, c-format
msgid ""
"Usage: %s [-c CSSFILE] [-d DELAY] [-t TIMEOUT] [--force] [-w WIDTH] [--print-"
"background] [--file] URI|FILE OUTFILE|--printer=PRINTER\n"
msgstr ""

#: ../src/gnome-web-photo.c:838
#, c-format
msgid ""
"Usage: %s [-c CSSFILE] [-d DELAY] [-t TIMEOUT] [--force] [-w WIDTH] [--print-"
"background] [--file] URI|FILE OUTFILE\n"
msgstr ""

#. Translators: the leading spaces are a way to add tabulation in some text
#. * printed on the terminal; %s is the name of a printer.
#: ../src/gnome-web-photo.c:878
#, c-format
msgid "  %s\n"
msgstr ""

#. Translators: the leading spaces are a way to add tabulation in some text
#. * printed on the terminal; %s is the name of a printer; "active" applies
#. * to the printer.
#: ../src/gnome-web-photo.c:883
#, c-format
msgid "  %s (not active)\n"
msgstr ""

#: ../src/gnome-web-photo.c:913
msgid "Operation mode [photo|thumbnail|print]"
msgstr ""

#: ../src/gnome-web-photo.c:915
msgid "User style sheet to use for the page (default: "
msgstr ""

#. Translators: CSSFILE will appear in the help, as in: --user-css=CSSFILE
#: ../src/gnome-web-photo.c:917
msgid "CSSFILE"
msgstr ""

#: ../src/gnome-web-photo.c:919
msgid ""
"Delay in seconds to wait after page is loaded, or 0 to disable delay "
"(default: 0)"
msgstr ""

#. Translators: T will appear in the help, as in: --delay=D
#: ../src/gnome-web-photo.c:921
msgid "D"
msgstr ""

#: ../src/gnome-web-photo.c:923
msgid ""
"Timeout in seconds to wait to load the page, or 0 to disable timeout "
"(default: 60)"
msgstr ""

#. Translators: T will appear in the help, as in: --timeout=T
#: ../src/gnome-web-photo.c:925
msgid "T"
msgstr ""

#: ../src/gnome-web-photo.c:927
msgid "Force output when timeout expires, even if the page is not fully loaded"
msgstr ""

#: ../src/gnome-web-photo.c:929
msgid "Desired width of the web page (default: 1024)"
msgstr ""

#. Translators: W will appear in the help, as in: --width=W
#: ../src/gnome-web-photo.c:931
msgid "W"
msgstr ""

#: ../src/gnome-web-photo.c:933
msgid "Thumbnail size (default: 256)"
msgstr ""

#. Translators: S will appear in the help, as in: --thumbnail-size=S
#: ../src/gnome-web-photo.c:935
msgid "S"
msgstr ""

#: ../src/gnome-web-photo.c:938
msgid "Print page on PRINTER (default: none, save as PDF)"
msgstr ""

#. Translators: PRINTER will appear in the help, as in: --printer=PRINTER
#: ../src/gnome-web-photo.c:940
msgid "PRINTER"
msgstr ""

#: ../src/gnome-web-photo.c:943
msgid "Print background images and colours (default: false)"
msgstr ""

#: ../src/gnome-web-photo.c:945
msgid ""
"Disable embedded plugins in the rendering engine (default: enable plugins)"
msgstr ""

#: ../src/gnome-web-photo.c:947
msgid "Argument is a file and not a URI"
msgstr ""

#. Translators: %s is a filename or a URI
#: ../src/gnome-web-photo.c:1012
#, c-format
msgid "Specified user stylesheet ('%s') does not exist!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1033
#, c-format
msgid "--delay cannot be negative!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1040
#, c-format
msgid "--timeout cannot be negative!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1055
#, c-format
msgid "--size can only be 32, 64, 96, 128 or 256!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1059
#, c-format
msgid "--size is only available in thumbnail mode!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1068
#, c-format
msgid "--width out of bounds; must be between %d and %d!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1073
#, c-format
msgid "--width must be a multiple of 32 in thumbnail mode!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1081
#, c-format
msgid "--print-background is only available in print mode!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1089
#, c-format
msgid "--printer is only available in print mode!\n"
msgstr ""

#. Translators: %s is the name of a printer
#: ../src/gnome-web-photo.c:1098
#, c-format
msgid "'%s' is not a valid printer!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1100
#, c-format
msgid "List of printers:\n"
msgstr ""

#: ../src/gnome-web-photo.c:1134
#, c-format
msgid "Missing arguments!\n"
msgstr ""

#: ../src/gnome-web-photo.c:1140
#, c-format
msgid "Too many arguments!\n"
msgstr ""

#: ../src/photo-offscreen-window.c:164
msgid "Maximum width"
msgstr ""

#: ../src/photo-offscreen-window.c:165
msgid "Maximum width of the offscreen window"
msgstr ""

#: ../src/photo-offscreen-window.c:182
msgid "Maximum height"
msgstr ""

#: ../src/photo-offscreen-window.c:183
msgid "Maximum height of the offscreen window"
msgstr ""
