# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=gitg&keywords=I18N+L10N&component=gitg\n"
"POT-Creation-Date: 2012-11-09 11:50+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/gitg.desktop.in.in.h:1 ../gitg/gitg.vala:35
#: ../gitg/resources/ui/gitg-window.ui.h:1
msgid "gitg"
msgstr ""

#: ../data/gitg.desktop.in.in.h:2
msgid "Git repository browser"
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:1
msgid "Filter Revisions When Searching"
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:2
msgid ""
"Whether searching filters the revisions in the history view instead of "
"jumping to the first match."
msgstr ""

#. ex: ts=4 noet
#: ../data/org.gnome.gitg.gschema.xml.in.in.h:3
#: ../plugins/history/org.gnome.gitg.history.gschema.xml.in.in.h:1
msgid "When to Collapse Inactive Lanes"
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:4
#: ../plugins/history/org.gnome.gitg.history.gschema.xml.in.in.h:2
msgid ""
"Setting that indicates when an inactive lane should be collapsed. Valid "
"values are 0 - 4, where 0 indicates 'early' and 4 indicates 'late'."
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:5
#: ../plugins/history/org.gnome.gitg.history.gschema.xml.in.in.h:4
msgid "Show History in Topological Order"
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:6
#: ../plugins/history/org.gnome.gitg.history.gschema.xml.in.in.h:5
msgid ""
"Setting that indicates whether to show the history in topological order."
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:7
msgid "Orientation of the main interface (vertical or horizontal)"
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:8
msgid "Setting that sets the orientation of the main interface."
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:9
msgid "Show Right Margin in Commit Message View"
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:10
msgid ""
"Show a right margin indicator in the commit message view. This can be used "
"to easily see where to break the commit message at a particular column."
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:11
msgid "Column at Which Right Margin is Shown"
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:12
msgid ""
"The column at which the right margin is shown if the right-margin preference "
"is set to TRUE."
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:13
msgid "Allow External Diff Program"
msgstr ""

#: ../data/org.gnome.gitg.gschema.xml.in.in.h:14
msgid "Allow an external diff program to be used when viewing diffs in gitg."
msgstr ""

#: ../gitg/gitg-application.vala:43
msgid "Show the application's version"
msgstr ""

#: ../gitg/gitg-application.vala:45
msgid "Start gitg with a particular view"
msgstr ""

#: ../gitg/gitg-application.vala:47
msgid "Do not try to load a repository from the current working directory"
msgstr ""

#: ../gitg/gitg-application.vala:70
msgid "- git repository viewer"
msgstr ""

#: ../gitg/gitg-application.vala:185
msgid "gitg is a git repository viewer for gtk+/GNOME"
msgstr ""

#: ../gitg/gitg-application.vala:210
msgid "gitg homepage"
msgstr ""

#: ../gitg/gitg-preferences-interface.vala:83
msgid "Interface"
msgstr ""

#. ex:set ts=4 noet
#: ../gitg/resources/ui/gitg-menus.ui.h:1
msgid "_New Window"
msgstr ""

#: ../gitg/resources/ui/gitg-menus.ui.h:2
msgid "_Preferences"
msgstr ""

#: ../gitg/resources/ui/gitg-menus.ui.h:3
msgid "_Help"
msgstr ""

#: ../gitg/resources/ui/gitg-menus.ui.h:4
msgid "_About"
msgstr ""

#: ../gitg/resources/ui/gitg-menus.ui.h:5
msgid "_Quit"
msgstr ""

#: ../gitg/resources/ui/gitg-menus.ui.h:6
msgid "_Close"
msgstr ""

#: ../gitg/resources/ui/gitg-preferences-interface.ui.h:1
msgid "<b>Layout</b>"
msgstr ""

#: ../gitg/resources/ui/gitg-preferences-interface.ui.h:2
#: ../plugins/history/resources/preferences.ui.h:2
msgid "    "
msgstr ""

#: ../gitg/resources/ui/gitg-preferences-interface.ui.h:3
msgid "Use horizontal layout"
msgstr ""

#: ../gitg/resources/ui/gitg-preferences.ui.h:1
msgid "gitg Preferences"
msgstr ""

#: ../plugins/dash/gitg-dash-navigation.vala:41
msgid "Repository"
msgstr ""

#: ../plugins/dash/gitg-dash-navigation.vala:42
msgid "Open"
msgstr ""

#: ../plugins/dash/gitg-dash-navigation.vala:43
#: ../plugins/dash/resources/view-create.ui.h:3
msgid "Create"
msgstr ""

#: ../plugins/dash/gitg-dash-navigation.vala:46
msgid "Recent"
msgstr ""

#: ../plugins/dash/gitg-dash.vala:69
msgid "Dashboard"
msgstr ""

#: ../plugins/dash/gitg-dash.vala:249
msgid "(no branch)"
msgstr ""

#. ex: ts=4 noet
#: ../plugins/dash/resources/view-create.ui.h:1
msgid "Create repository"
msgstr ""

#: ../plugins/dash/resources/view-create.ui.h:2
msgid "Select a folder in which you want to create a new git repository."
msgstr ""

#: ../plugins/dash/resources/view-open.ui.h:1
msgid "Open repository"
msgstr ""

#: ../plugins/dash/resources/view-open.ui.h:2
msgid "Select a folder to open the corresponding git repository."
msgstr ""

#: ../plugins/dash/resources/view-recent.ui.h:1
msgid "Recent repository"
msgstr ""

#: ../plugins/dash/resources/view-recent.ui.h:2
msgid "Open a recently used repository."
msgstr ""

#: ../plugins/dash/resources/view-recent.ui.h:3
msgid "Path:"
msgstr ""

#: ../plugins/dash/resources/view-recent.ui.h:4
msgid "Last used:"
msgstr ""

#: ../plugins/dash/resources/view-recent.ui.h:5
msgid "Current branch:"
msgstr ""

#: ../plugins/diff/gitg-diff.vala:63
msgid "Diff"
msgstr ""

#: ../plugins/files/gitg-files.vala:72
msgid "Files"
msgstr ""

#. ex: ts=4 noet
#: ../plugins/files/resources/view-files.ui.h:1
msgid "column"
msgstr ""

#: ../plugins/history/gitg-history-command-line.vala:31
msgid "Show all history"
msgstr ""

#: ../plugins/history/gitg-history-command-line.vala:38
msgid "Show gitg history options"
msgstr ""

#: ../plugins/history/gitg-history-command-line.vala:39
msgid "gitg history options"
msgstr ""

#: ../plugins/history/gitg-history-navigation.vala:120
#: ../plugins/history/gitg-history-navigation.vala:124
msgid "All commits"
msgstr ""

#. Branches
#: ../plugins/history/gitg-history-navigation.vala:128
msgid "Branches"
msgstr ""

#. Remotes
#: ../plugins/history/gitg-history-navigation.vala:163
msgid "Remotes"
msgstr ""

#. Tags
#: ../plugins/history/gitg-history-navigation.vala:188
msgid "Tags"
msgstr ""

#: ../plugins/history/gitg-history-preferences.vala:136
#: ../plugins/history/gitg-history.vala:135
msgid "History"
msgstr ""

#: ../plugins/history/org.gnome.gitg.history.gschema.xml.in.in.h:3
msgid ""
"Setting that indicates whether to collapse history lanes which do not show "
"activity. Enabling this can provide a cleaner history view when there is a "
"lot of parallel development. See collapse-inactive-lanes to control when "
"lanes should be collapsed."
msgstr ""

#: ../plugins/history/org.gnome.gitg.history.gschema.xml.in.in.h:6
msgid ""
"Setting that indicates whether to show items for the stash in the history."
msgstr ""

#: ../plugins/history/org.gnome.gitg.history.gschema.xml.in.in.h:7
msgid ""
"Setting that indicates whether to show a virtual item for the currently "
"staged changes in the history."
msgstr ""

#: ../plugins/history/org.gnome.gitg.history.gschema.xml.in.in.h:8
msgid ""
"Setting that indicates whether to show a virtual item for the currently "
"unstaged changes in the history."
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:1
msgid "<b>Commits</b>"
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:3
msgid "Collapse inactive lanes"
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:4
msgid "Show stash in history"
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:5
msgid "Show staged changes in history"
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:6
msgid "Show unstaged changes in history"
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:7
msgid "Show history in topological order"
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:8
msgid "        "
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:9
msgid "Early"
msgstr ""

#: ../plugins/history/resources/preferences.ui.h:10
msgid "Late"
msgstr ""

#: ../plugins/history/resources/view-history.ui.h:1
msgid "Subject"
msgstr ""

#: ../plugins/history/resources/view-history.ui.h:2
msgid "Author"
msgstr ""

#: ../plugins/history/resources/view-history.ui.h:3
msgid "Date"
msgstr ""
