# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=ontv&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-09-16 15:20+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../data/90-ontv.xml.in.h:1 ../data/GNOME_OnTVApplet.server.in.in.h:3
#: ../data/ontv.desktop.in.h:1
msgid "OnTV"
msgstr ""

#: ../data/about_dialog.ui.h:1
msgid "Copyright (C) 2004-2010 Johan Svedberg"
msgstr ""

#: ../data/about_dialog.ui.h:2
msgid "OnTV is a GNOME Applet for monitoring TV programs."
msgstr ""

#: ../data/about_dialog.ui.h:3
msgid "translator-credits"
msgstr ""

#: ../data/GNOME_OnTVApplet.server.in.in.h:1
msgid "OnTV Applet Factory"
msgstr ""

#: ../data/GNOME_OnTVApplet.server.in.in.h:2
msgid "Factory for creating the OnTV applet"
msgstr ""

#: ../data/GNOME_OnTVApplet.server.in.in.h:4 ../data/ontv.desktop.in.h:2
msgid "Monitor TV programs"
msgstr ""

#: ../data/GNOME_OnTVApplet.server.in.in.h:5
msgid "Accessories"
msgstr ""

#: ../data/GNOME_OnTVApplet.xml.h:1
msgid "_Update TV listings"
msgstr ""

#: ../data/GNOME_OnTVApplet.xml.h:2
msgid "_Preferences"
msgstr ""

#: ../data/GNOME_OnTVApplet.xml.h:3
msgid "_Search Program"
msgstr ""

#: ../data/GNOME_OnTVApplet.xml.h:4
msgid "_About"
msgstr ""

#: ../data/ontv.schemas.in.in.h:1
msgid "XMLTV grabber command"
msgstr ""

#: ../data/ontv.schemas.in.in.h:2
msgid "Command for grabbing XMLTV file."
msgstr ""

#: ../data/ontv.schemas.in.in.h:3
msgid "Path to XMLTV file"
msgstr ""

#: ../data/ontv.schemas.in.in.h:4
msgid "Absolute path to the XMLTV file containing TV listings."
msgstr ""

#: ../data/ontv.schemas.in.in.h:5
msgid "Toggle program window binding"
msgstr ""

#: ../data/ontv.schemas.in.in.h:6
msgid "Keyboard shortcut for toggling visibility of the OnTV program window."
msgstr ""

#: ../data/ontv.schemas.in.in.h:7
msgid "Toggle program window action"
msgstr ""

#: ../data/ontv.schemas.in.in.h:8
msgid "Command for toggling visibility of the OnTV program window."
msgstr ""

#: ../data/ontv.schemas.in.in.h:9
msgid "Toggle OnTV program window"
msgstr ""

#: ../data/ontv.schemas.in.in.h:10
msgid "Toggle visibility of the OnTV program window."
msgstr ""

#: ../data/ontv.schemas.in.in.h:11
msgid "Show search program binding"
msgstr ""

#: ../data/ontv.schemas.in.in.h:12
msgid "Keyboard shortcut for showing the search program window."
msgstr ""

#: ../data/ontv.schemas.in.in.h:13
msgid "Show search program action"
msgstr ""

#: ../data/ontv.schemas.in.in.h:14
msgid "Command for showing the search program window."
msgstr ""

#: ../data/ontv.schemas.in.in.h:15
msgid "Show search program dialog"
msgstr ""

#: ../data/ontv.schemas.in.in.h:16
msgid "Show search program dialog."
msgstr ""

#: ../data/ontv.schemas.in.in.h:17
msgid "Display current programs"
msgstr ""

#: ../data/ontv.schemas.in.in.h:18
msgid "Whether OnTV should display current programs."
msgstr ""

#: ../data/ontv.schemas.in.in.h:19
msgid "Display upcoming programs"
msgstr ""

#: ../data/ontv.schemas.in.in.h:20
msgid "Whether OnTV should display upcoming programs."
msgstr ""

#: ../data/ontv.schemas.in.in.h:21
msgid "Position upcoming programs below or to the right"
msgstr ""

#: ../data/ontv.schemas.in.in.h:22
msgid ""
"Whether OnTV should position upcoming programs below or to the right of the "
"current programs."
msgstr ""

#: ../data/preferences_dialog.ui.h:1
msgid "Preferences for OnTV"
msgstr ""

#: ../data/preferences_dialog.ui.h:2 ../ontv/assistant.py:161
msgid "Browse..."
msgstr ""

#: ../data/preferences_dialog.ui.h:3
msgid "_Output file:"
msgstr ""

#: ../data/preferences_dialog.ui.h:4
msgid "_Grabber command:"
msgstr ""

#: ../data/preferences_dialog.ui.h:5
msgid "<b>XMLTV</b>"
msgstr ""

#: ../data/preferences_dialog.ui.h:6
msgid "C_urrent programs"
msgstr ""

#: ../data/preferences_dialog.ui.h:7
msgid "U_pcoming programs"
msgstr ""

#: ../data/preferences_dialog.ui.h:8
msgid "Position:"
msgstr ""

#: ../data/preferences_dialog.ui.h:9
msgid "_Below"
msgstr ""

#: ../data/preferences_dialog.ui.h:10
msgid "To the _right"
msgstr ""

#: ../data/preferences_dialog.ui.h:11
msgid "<b>Display</b>"
msgstr ""

#: ../data/preferences_dialog.ui.h:12
msgid "General"
msgstr ""

#: ../data/preferences_dialog.ui.h:13
msgid "_Select channels to monitor in preferred order:"
msgstr ""

#: ../data/preferences_dialog.ui.h:14
msgid "Channels"
msgstr ""

#: ../data/preferences_dialog.ui.h:15
msgid "_Current reminders:"
msgstr ""

#: ../data/preferences_dialog.ui.h:16
msgid "_Program:"
msgstr ""

#: ../data/preferences_dialog.ui.h:17
msgid "_Channel:"
msgstr ""

#: ../data/preferences_dialog.ui.h:18
msgid "_Notify"
msgstr ""

#: ../data/preferences_dialog.ui.h:19
msgid "minutes before the program begins"
msgstr ""

#: ../data/preferences_dialog.ui.h:20
msgid "Reminders"
msgstr ""

#: ../data/program_dialog.ui.h:1
msgid "<b>Name:</b>"
msgstr ""

#: ../data/program_dialog.ui.h:2
msgid "<b>Channel:</b>"
msgstr ""

#: ../data/program_dialog.ui.h:3
msgid "<b>Air time:</b>"
msgstr ""

#: ../data/program_dialog.ui.h:4
msgid "<b>Description:</b>"
msgstr ""

#: ../data/search_dialog.ui.h:1
msgid "Search Program"
msgstr ""

#: ../data/search_dialog.ui.h:2
msgid "_Search:"
msgstr ""

#: ../data/search_dialog.ui.h:3 ../ontv/gui.py:113
msgid "_Add reminder"
msgstr ""

#: ../data/search_dialog.ui.h:4 ../ontv/gui.py:108
msgid "_Details"
msgstr ""

#: ../data/status_icon.ui.h:1
msgid "Update TV Listings"
msgstr ""

#: ../data/status_icon.ui.h:2
msgid "Preferences"
msgstr ""

#: ../data/status_icon.ui.h:3
msgid "Search"
msgstr ""

#: ../data/status_icon.ui.h:4
msgid "About"
msgstr ""

#: ../data/status_icon.ui.h:5
msgid "Exit"
msgstr ""

#: ../ontv/assistant.py:34
msgid "Error while importing vte module"
msgstr ""

#: ../ontv/assistant.py:35
msgid "Could not find python-vte."
msgstr ""

#: ../ontv/assistant.py:54 ../ontv/assistant.py:77
msgid "OnTV XMLTV Configuration"
msgstr ""

#: ../ontv/assistant.py:71
msgid ""
"Welcome! This assistant will help you setup the program OnTV will use to "
"download TV listings for your country."
msgstr ""

#: ../ontv/assistant.py:91
msgid "Configure installed grabber"
msgstr ""

#: ../ontv/assistant.py:99
msgid "Country:"
msgstr ""

#: ../ontv/assistant.py:137
msgid "Use custom grabber"
msgstr ""

#: ../ontv/assistant.py:142
msgid "Grabber command:"
msgstr ""

#: ../ontv/assistant.py:152
msgid "Output file:"
msgstr ""

#: ../ontv/assistant.py:176 ../ontv/assistant.py:242
msgid "Configure grabber"
msgstr ""

#: ../ontv/assistant.py:217 ../ontv/dialogs.py:425
msgid "XML files"
msgstr ""

#: ../ontv/assistant.py:220 ../ontv/dialogs.py:428
msgid "Select XMLTV file..."
msgstr ""

#: ../ontv/assistant.py:233
msgid "Configure the grabber by answering the questions in the terminal below."
msgstr ""

#: ../ontv/assistant.py:250
msgid ""
"Please wait while the TV listings are being downloaded and sorted. This may "
"take several minutes depending on the speed of your Internet connection."
msgstr ""

#: ../ontv/assistant.py:257
msgid "Details"
msgstr ""

#: ../ontv/assistant.py:263
msgid "Downloading and sorting TV listings"
msgstr ""

#: ../ontv/assistant.py:271
msgid ""
"Congratulations! OnTV is now ready for use. You can select which channels to "
"monitor in the preferences dialog."
msgstr ""

#: ../ontv/assistant.py:277
msgid "All done!"
msgstr ""

#: ../ontv/assistant.py:386
msgid "Downloading TV listings..."
msgstr ""

#: ../ontv/assistant.py:434
msgid "Failed to download TV listings!"
msgstr ""

#: ../ontv/assistant.py:447
msgid "Sorting TV listings..."
msgstr ""

#: ../ontv/assistant.py:464
msgid "Done!"
msgstr ""

#: ../ontv/channel.py:117
#, python-format
msgid "Error while loading %s"
msgstr ""

#: ../ontv/config.py:75
msgid "Could not find configuration directory in GConf"
msgstr ""

#: ../ontv/config.py:75
msgid "Please make sure that ontv.schemas was correctly installed."
msgstr ""

#: ../ontv/dialogs.py:83
#, python-format
msgid "Properties for channel %s"
msgstr ""

#: ../ontv/dialogs.py:168 ../ontv/dialogs.py:675 ../ontv/dialogs.py:792
#: ../ontv/dialogs.py:840 ../ontv/listings.py:108 ../ontv/reminders.py:59
msgid "All"
msgstr ""

#: ../ontv/dialogs.py:288
msgid "Program"
msgstr ""

#: ../ontv/dialogs.py:296
msgid "Channel"
msgstr ""

#: ../ontv/dialogs.py:304
msgid "Notification time"
msgstr ""

#: ../ontv/dialogs.py:711
#, python-format
msgid "Details about %s"
msgstr ""

#: ../ontv/gui.py:145
msgid "All files"
msgstr ""

#: ../ontv/gui.py:153
msgid "Error"
msgstr ""

#: ../ontv/notify.py:34
msgid "Error while importing pynotify module"
msgstr ""

#: ../ontv/notify.py:35
msgid "Could not find python-notify."
msgstr ""

#: ../ontv/notify.py:68
#, python-format
msgid "%s until the program begins."
msgstr ""

#: ../ontv/ontv_core.py:102
msgid "Downloading TV Listings..."
msgstr ""

#: ../ontv/ontv_core.py:106
msgid "Sorting TV Listings..."
msgstr ""

#: ../ontv/ontv_core.py:110
msgid "Loading TV Listings..."
msgstr ""

#: ../ontv/window.py:78
msgid "No channels selected"
msgstr ""

#: ../ontv/window.py:160
msgid "Now Playing..."
msgstr ""

#: ../ontv/window.py:190
#, python-format
msgid "%d hour"
msgid_plural "%d hours"
msgstr[0] ""
msgstr[1] ""

#: ../ontv/window.py:191
#, python-format
msgid "%d minute"
msgid_plural "%d minutes"
msgstr[0] ""
msgstr[1] ""

#: ../ontv/window.py:192
#, python-format
msgid "%s and %s left"
msgstr ""

#: ../ontv/window.py:194
#, python-format
msgid "%d hour left"
msgid_plural "%d hours left"
msgstr[0] ""
msgstr[1] ""

#: ../ontv/window.py:197
#, python-format
msgid "%d minute left"
msgid_plural "%d minutes left"
msgstr[0] ""
msgstr[1] ""

#: ../ontv/window.py:200
#, python-format
msgid "%d second left"
msgid_plural "%d seconds left"
msgstr[0] ""
msgstr[1] ""

#: ../ontv/window.py:233
msgid "Upcoming programs"
msgstr ""

#: ../ontv/xmltv_file.py:98
#, python-format
msgid "Failed to load %s"
msgstr ""

#: ../ontv/xmltv_file.py:100
msgid "File not found"
msgstr ""

#: ../ontv/xmltv_file.py:102
msgid "Access denied"
msgstr ""

#: ../ontv/xmltv_file.py:104
msgid "Unknown error"
msgstr ""

#: ../ontv/xmltv_file.py:106
#, python-format
msgid "Error while parsing %s"
msgstr ""

#: ../ontv/xmltv_file.py:107
#, python-format
msgid "The parser returned: \"%s\""
msgstr ""

#: ../ontv/xmltv_file.py:111
#, python-format
msgid "Unknown error while loading %s"
msgstr ""
