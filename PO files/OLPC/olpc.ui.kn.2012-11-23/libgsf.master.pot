# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=libgsf&keywords=I18N+L10N&component=General\n"
"POT-Creation-Date: 2012-11-18 16:21+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../gsf/gsf-blob.c:115
#, c-format
msgid "Not enough memory to copy %s bytes of data"
msgstr ""

#: ../gsf/gsf-clip-data.c:166
#, c-format
msgid "The clip_data is in %s, but it is smaller than at least %s bytes"
msgstr ""

#: ../gsf/gsf-clip-data.c:260
#, c-format
msgid ""
"The clip_data is in Windows clipboard format, but it is smaller than the "
"required 4 bytes."
msgstr ""

#: ../gsf/gsf-clip-data.c:271
msgid "Windows Metafile format"
msgstr ""

#. CF_BITMAP
#: ../gsf/gsf-clip-data.c:277
msgid "Windows DIB or BITMAP format"
msgstr ""

#: ../gsf/gsf-clip-data.c:282
msgid "Windows Enhanced Metafile format"
msgstr ""

#: ../gsf/gsf-libxml.c:1493
msgid "Pretty print"
msgstr ""

#: ../gsf/gsf-libxml.c:1494
msgid "Should the output auto-indent elements to make reading easier?"
msgstr ""

#: ../gsf/gsf-libxml.c:1500
msgid "Sink"
msgstr ""

#: ../gsf/gsf-libxml.c:1501
msgid "The destination for writes"
msgstr ""

#: ../gsf/gsf-msole-utils.c:315
#, c-format
msgid ""
"Missing data when reading the %s property; got %s bytes, but %s bytes at "
"least are needed."
msgstr ""

#: ../gsf/gsf-msole-utils.c:366
#, c-format
msgid ""
"Corrupt data in the VT_CF property; clipboard data length must be at least 4 "
"bytes, but the data says it only has %s bytes available."
msgstr ""

#: ../gsf/gsf-open-pkg-utils.c:355
#, c-format
msgid "Unable to find part id='%s' for '%s'"
msgstr ""

#: ../gsf/gsf-open-pkg-utils.c:383
#, c-format
msgid "Unable to find part with type='%s' for '%s'"
msgstr ""

#: ../gsf/gsf-open-pkg-utils.c:413
#, c-format
msgid "Missing id for part in '%s'"
msgstr ""

#: ../gsf/gsf-open-pkg-utils.c:422
#, c-format
msgid "Part '%s' in '%s' from '%s' is corrupt!"
msgstr ""

#: ../gsf/gsf-opendoc-utils.c:353
#, c-format
msgid "Property \"%s\" used for multiple types!"
msgstr ""

#: ../gsf/gsf-opendoc-utils.c:878
msgid "ODF version"
msgstr ""

#: ../gsf/gsf-opendoc-utils.c:879
msgid "The ODF version this object is targeting as an integer like 100"
msgstr ""

#: ../tools/gsf.c:27
msgid "Display program version"
msgstr ""

#: ../tools/gsf.c:54
#, c-format
msgid "%s: Failed to open %s: %s\n"
msgstr ""

#: ../tools/gsf.c:75
#, c-format
msgid "%s: Failed to recognize %s as an archive\n"
msgstr ""

#: ../tools/gsf.c:112
#, c-format
msgid "Available subcommands are...\n"
msgstr ""

#: ../tools/gsf.c:113
#, c-format
msgid "* cat        output one or more files in archive\n"
msgstr ""

#: ../tools/gsf.c:114
#, c-format
msgid "* dump       dump one or more files in archive as hex\n"
msgstr ""

#: ../tools/gsf.c:115
#, c-format
msgid "* help       list subcommands\n"
msgstr ""

#: ../tools/gsf.c:116
#, c-format
msgid "* list       list files in archive\n"
msgstr ""

#: ../tools/gsf.c:117
#, c-format
msgid "* listprops  list document properties in archive\n"
msgstr ""

#: ../tools/gsf.c:118
#, c-format
msgid "* props      print specified document properties\n"
msgstr ""

#: ../tools/gsf.c:119
#, c-format
msgid "* createole  create OLE archive\n"
msgstr ""

#: ../tools/gsf.c:120
#, c-format
msgid "* createzip  create ZIP archive\n"
msgstr ""

#: ../tools/gsf.c:303
#, c-format
msgid "No property named %s\n"
msgstr ""

#: ../tools/gsf.c:362
#, c-format
msgid "%s: Error processing file %s: %s\n"
msgstr ""

#: ../tools/gsf.c:526
msgid "SUBCOMMAND ARCHIVE..."
msgstr ""

#: ../tools/gsf.c:533
#, c-format
msgid ""
"%s\n"
"Run '%s --help' to see a full list of available command line options.\n"
msgstr ""

#: ../tools/gsf.c:540
#, c-format
msgid "gsf version %d.%d.%d\n"
msgstr ""

#: ../tools/gsf.c:546
#, c-format
msgid "Usage: %s %s\n"
msgstr ""

#: ../tools/gsf.c:571
#, c-format
msgid "Run '%s help' to see a list subcommands.\n"
msgstr ""
